package homeWorks.hw1;

public class Main {

    public static void main(String[] args) {
        printText(); // run method printText()
        int a = 2, b = 2;
        System.out.println(sum(a, b)); // run method sum and pass two arguments

         a = -2;
         b = 2;
        System.out.println(sum(a, b));

        float a2 = 2.5f;
        float b2 = 2.5f;
        System.out.println(sumOfFloats(a2, b2)); //run method sum and pass two float arguments

        a = 2_000_000_000;
        b = 2_000_000_00;
        System.out.println(sum(a, b));
    }

    public static void printText() { // run method
        System.out.println("Hello, DevEducation!"); // and it`s print some text
    }

    public static float sumOfFloats(float a, float b) { //run method and accept two float numbers
        return a + b; // add two float numbers and return them
    }

    public static int sum(int a, int b) {
        return a + b; //add two numbers and return them
    }

    /*
     * this method is test-method for checking right logic
     * our program
     */
    public void additionShouldBePositive() {
        float expected = 5.0f;
        float result = sumOfFloats(2.5f, 2.5f);
    }
}
